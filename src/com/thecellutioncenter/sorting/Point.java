package com.thecellutioncenter.sorting;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class Point extends Rectangle {

	public static int nextX = 0;
	public static int pointSize = 2;
	private int value;

	public boolean isSelected;

	public Point() {
		width = pointSize;
		height = pointSize;
		
		Random r = new Random();
		x = nextX;
		nextX += 2;
		
		y = Screen.myHeight - height - r.nextInt(280);
		setValue(Screen.myHeight - y);
	}

	public void setSelected(boolean selected) {
		isSelected = selected;
	}

	public void setX(int x) {
		setBounds(x, y, width, height);
	}

	public void setY(int y) {
		setBounds(x, y, width, height);
	}

	public void draw(Graphics g) {
		/* TODO: Everything would be much easier if these were drawn according
		 * to their position in the array/list instead of given coordinates.
		 */
		if (isSelected) {
			g.setColor(Color.red);
		} else {
			g.setColor(new Color(100, 100, 255));
		}
		g.fillRect(x, y, width, Screen.myHeight - y);
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
