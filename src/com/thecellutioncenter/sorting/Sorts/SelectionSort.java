package com.thecellutioncenter.sorting.Sorts;

import com.thecellutioncenter.sorting.ClickListening;
import com.thecellutioncenter.sorting.Screen;
import com.thecellutioncenter.sorting.Stats;

public class SelectionSort extends Sort {

	public void sort() {
		Stats.setSortType("Selection Sort");
		// # comparison algorithm: numPoints * (numPoints - 1) / 2
		// # of swaps should be <= numPoints

		for (int j = 0; j < Screen.points.length; j++) {

			if (!Screen.isTiming){
				break;
			}
			
			int min = j;
			for (int i = j + 1; i < Screen.points.length; i++) {
				
				Screen.points[i].setSelected(true);
				
				if (Screen.points[i].getY() > Screen.points[min].getY()){ //backwards because checking from bottom
					Screen.points[min].setSelected(false);
					min = i;
					Screen.points[min].setSelected(true);
				}
				Stats.addComparisons();

				try {
					Thread.sleep(ClickListening.sortSpeed);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				if (i != min) {
					Screen.points[i].setSelected(false);
				}
			}

			if (min != j) {
				// inherited method
				swapPoints(min, j);

				Stats.addSwaps();
				
				Screen.points[j].setSelected(false);
			}
		}
	}

}
