package com.thecellutioncenter.sorting.Sorts;

import java.util.ArrayList;
import java.util.Arrays;

import com.thecellutioncenter.sorting.Point;
import com.thecellutioncenter.sorting.Screen;
import com.thecellutioncenter.sorting.Stats;

public class QuickSort extends Sort {

	@Override
	public void sort() {
		Stats.setSortType("Quick Sort");
		
		quickSort(new ArrayList<Point>(Arrays.asList(Screen.points)));
	}
	
	public ArrayList<Point> quickSort(ArrayList<Point> points){
		if(points.size() < 2){
			return points;
		}
		
		ArrayList<Point> better = new ArrayList<Point>();
		ArrayList<Point> worse = new ArrayList<Point>();
		
		int lowest = 0;
		int highest = points.size();
		
		/* TODO: Pick better pivot */
		Point pivot = points.get(0);
		
		for(Point point : points){
			if (point.getValue() < pivot.getValue()){
				worse.add(point);
			} else {
				better.add(point);
			}
		}
		
		worse = quickSort(worse);
		better = quickSort(better);
		
		ArrayList<Point> result = new ArrayList<Point>();
		result.addAll(worse);
		result.add(pivot);
		result.addAll(better);
		
		return result;
	}

}
