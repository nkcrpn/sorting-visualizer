package com.thecellutioncenter.sorting.Sorts;

import com.thecellutioncenter.sorting.ClickListening;
import com.thecellutioncenter.sorting.Screen;
import com.thecellutioncenter.sorting.Stats;

public class BubbleSort extends Sort {
	
	public void sort(){
		Stats.setSortType("Bubble Sort");
		
		boolean swapped = true;
		int j = 0;
		
		while(swapped){
			swapped = false;
			j++;
			
			if (!Screen.isTiming){
				break;
			}
			
			for (int i = 0; i < Screen.points.length - j; i++){
				Screen.points[i].setSelected(true);
				Screen.points[i+1].setSelected(true);
				
				if (Screen.points[i].getY() < Screen.points[i+1].getY()){ //backwards because drawing from bottom of screen
					
					// inherited method
					swapPoints(i, i+1);
					Stats.addSwaps();
					swapped = true;
				}
				
				Stats.addComparisons();
				
				try {
					Thread.sleep(ClickListening.sortSpeed);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				Screen.points[i].setSelected(false);
				Screen.points[i+1].setSelected(false);
			}
		}
		
	}
	
}
