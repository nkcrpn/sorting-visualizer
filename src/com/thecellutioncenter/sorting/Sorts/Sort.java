package com.thecellutioncenter.sorting.Sorts;

import com.thecellutioncenter.sorting.Point;
import com.thecellutioncenter.sorting.Screen;
import com.thecellutioncenter.sorting.Stats;

public abstract class Sort implements Runnable {
	
	@Override
	public void run() {
		Screen.isTiming = true;
		sort();
		Screen.isTiming = false;
	}
	
	public void swapPoints(int pos1, int pos2){
		
		int x = (int) Screen.points[pos1].getX();
		Point temp = Screen.points[pos1];

		Screen.points[pos1].setX((int) Screen.points[pos2].getX());
		Screen.points[pos1] = Screen.points[pos2];

		Screen.points[pos2].setX(x);
		Screen.points[pos2] = temp;
	}
	
	public abstract void sort();
	
}
