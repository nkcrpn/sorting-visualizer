package com.thecellutioncenter.sorting.Sorts;

import com.thecellutioncenter.sorting.ClickListening;
import com.thecellutioncenter.sorting.Point;
import com.thecellutioncenter.sorting.Screen;
import com.thecellutioncenter.sorting.Stats;

public class InsertionSort extends Sort {

	public void sort() {
		Stats.setSortType("Insertion Sort");

		// start with next number, compare to previous until no longer smaller
		int counter = 0, newY;
		for (int i = 1; i < Screen.points.length; i++) {
			
			if (!Screen.isTiming){
				break;
			}

			Point tempP = Screen.points[i];
			newY = (int) Screen.points[i].getY();

			if (i < Screen.points.length - 1) {
				Screen.points[i + 1].setSelected(true);
			}

			counter = i;
			while (counter > 0 && Screen.points[counter - 1].getY() < newY) {
				Stats.addComparisons();
				Screen.points[counter - 1].setSelected(true);

				Screen.points[counter] = Screen.points[counter - 1];
				Screen.points[counter].setX((int) Screen.points[counter].getX()
						+ Point.pointSize);

				try {
					Thread.sleep(ClickListening.sortSpeed);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				Screen.points[counter].setSelected(false);
				counter--;

			}
			if (counter != 0) {
				Stats.addComparisons();
			}
			Screen.points[counter] = tempP;
			Screen.points[counter].setX(counter * Point.pointSize);
			Stats.addSwaps();
			Screen.points[counter].setSelected(false);
		}

	}

}
