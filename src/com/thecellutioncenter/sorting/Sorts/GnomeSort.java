package com.thecellutioncenter.sorting.Sorts;

import com.thecellutioncenter.sorting.ClickListening;
import com.thecellutioncenter.sorting.Screen;
import com.thecellutioncenter.sorting.Stats;

public class GnomeSort extends Sort {

	public void sort() {
		Stats.setSortType("Gnome Sort");

		int counter = 0;

		for (int i = 1; i < Screen.points.length; i++) {
			if (!Screen.isTiming){
				break;
			}
			
			counter = i;
			if (i < Screen.points.length - 1) {
				Screen.points[i + 1].setSelected(true);
			}
			while (counter > 0
					&& Screen.points[counter - 1].getY() < Screen.points[counter]
							.getY()) { // backwards
				Stats.addComparisons();
				Screen.points[counter].setSelected(true);

				swapPoints(counter, counter - 1);
				Stats.addSwaps();
				counter--;

				try {
					Thread.sleep(ClickListening.sortSpeed);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Screen.points[counter].setSelected(false);
			}
			if (counter != 0) {
				Stats.addComparisons();
			}
			if (i < Screen.points.length - 1) {
				Screen.points[i + 1].setSelected(false);
			}

		}

	}

}
