package com.thecellutioncenter.sorting;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.thecellutioncenter.sorting.Sorts.BubbleSort;
import com.thecellutioncenter.sorting.Sorts.GnomeSort;
import com.thecellutioncenter.sorting.Sorts.InsertionSort;
import com.thecellutioncenter.sorting.Sorts.SelectionSort;

public class ClickListening implements ActionListener {
	
	public static int sortSpeed;
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(Frame.btnReset)) {
			Screen.isTiming = false;
			Screen.newPoints();
			Stats.reset();
		} else if (e.getSource().equals(Frame.btnStop)){
			Screen.isTiming = false;
		} else if (e.getSource().equals(Frame.btnStart) && !Screen.isTiming){
			//start selected sort
			setSortSpeed();
			Thread thread = new Thread();
			switch(Frame.comSorts.getSelectedIndex()){
			case 0:
				thread = new Thread(new SelectionSort());
				break;
			case 1:
				thread = new Thread(new BubbleSort());
				break;
			case 2:
				thread = new Thread(new InsertionSort());
				break;
			case 3:
				thread = new Thread(new GnomeSort());
				break;
			}
			thread.start();
		}
	}
	
	public void setSortSpeed(){
		switch(Frame.comSpeeds.getSelectedIndex()){
		case 0:
			Screen.sleepTime = 20;
			sortSpeed = 20;
			break;
		case 1:
			Screen.sleepTime = 20;
			sortSpeed = 10;
			break;
		case 2:
			Screen.sleepTime = 20;
			sortSpeed = 1;
			break;
		case 3:
			Screen.sleepTime = 5;
			sortSpeed = 0;
			break;
		}
	}

}
