package com.thecellutioncenter.sorting;

import java.awt.BorderLayout;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;


public class Frame extends JApplet {
	
	// for JFrame: static Frame frame;
	static Screen screen;
	
	static int myWidth;
	static int myHeight;
	
	static JButton btnReset, btnStop, btnStart;
	
	static JComboBox<String> comSorts, comSpeeds;
	
	public static void main(String[] args){
		//frame = new Frame();
		//frame.go();
	}
	
	public void init(){
		super.init();
		go();
	}
	
	public void go(){
		
		//for a JFrame
		//setLocationRelativeTo(null);
		//setVisible(true);
		//setResizable(false);
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setSize(750, 500);
		myWidth = getWidth();
		myHeight = getHeight();
		
		screen = new Screen(this);
		add(screen, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		
		ClickListening listener = new ClickListening();
		btnReset = new JButton("Reset");
		btnReset.addActionListener(listener);
		btnStop = new JButton("Stop");
		btnStop.addActionListener(listener);
		btnStart = new JButton("Start");
		btnStart.addActionListener(listener);
		
		panel.add(btnReset);
		panel.add(btnStop);
		
		comSorts = new JComboBox<String>();
		comSorts.addItem("Selection");
		comSorts.addItem("Bubble");
		comSorts.addItem("Insertion");
		comSorts.addItem("Gnome");
		
		panel.add(comSorts);
		
		comSpeeds = new JComboBox<String>();
		comSpeeds.addItem("Slow");
		comSpeeds.addItem("Medium");
		comSpeeds.addItem("Fast");
		comSpeeds.addItem("Real-Time");
		comSpeeds.setSelectedIndex(2);
		
		panel.add(comSpeeds);
		panel.add(btnStart);
		
		add(panel, BorderLayout.NORTH);
		
	}
	
}
