package com.thecellutioncenter.sorting;

import java.awt.Color;
import java.awt.Graphics;


public class Stats {
	
	private static long millis;
	private static int swaps, comparisons;
	private static String sortType;
	
	public Stats(){
		millis = 0;
		swaps = 0;
		comparisons = 0;
		sortType = "Make a selection";
	}
	
	public static void reset(){
		millis = 0;
		swaps = 0;
		comparisons = 0;
		sortType = "Make a selection";
	}
	
	public void updateTime(long timePassed){
		millis += timePassed;
	}
	
	public static void addSwaps(){
		swaps+= 1;
	}
	
	public static void addComparisons(){
		comparisons+=1;
	}
	
	public static void setSortType(String type){
		sortType = type;
	}
	
	public void draw(Graphics g){
		g.setColor(new Color(100, 100, 255));
		String formatted = String.format("Time: %02d:%03d", millis/1000, millis%1000);
		g.drawString(formatted, 10, 20);
		
		g.drawString("Comparisons: " + comparisons, 10, 40);
		
		g.drawString("Swaps: " + swaps, 10, 60);
		
		g.drawString("Points: " + Screen.numPoints, Screen.myWidth - 80, 20);
		
		g.drawString(sortType, Screen.myWidth / 2 - 30, 20);
	}
	
}