package com.thecellutioncenter.sorting;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Screen extends JPanel implements Runnable {

	boolean isRunning, isFirst;
	public static boolean isTiming;
	
	static int myWidth, myHeight;
	static int numPoints;
	static int sleepTime;

	static Stats stats;
	
	public static Point[] points;

	public Screen(Frame frame) {
		isFirst = true;
	}
	
	public void define(){
		
		myWidth = Frame.screen.getWidth();
		myHeight = Frame.screen.getHeight();
		
		sleepTime = 20;
		
		numPoints = Frame.myWidth / 2;

		points = new Point[numPoints];
		newPoints();
		
		stats = new Stats();
		
		isTiming = false;
		isRunning = true;
		Thread thread = new Thread(this);
		thread.start();
	}
	
	public static void newPoints() {
		Point.nextX = 0;
		for (int i = 0; i < numPoints; i++) {
			points[i] = new Point();
		}
	}

	@Override
	public void run() {
		gameLoop();
	}

	public void gameLoop() {
		long cumTime = System.currentTimeMillis();

		while (isRunning) {
			long timePassed = System.currentTimeMillis() - cumTime;
			cumTime += timePassed;

			if (isTiming) {
				stats.updateTime(timePassed);
			}

			repaint();

			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		if (isFirst){
			define();
			isFirst = false;
		}

		g.setColor(Color.black);
		g.fillRect(0, 0, getWidth(), getHeight());

		for (Point p : points) {
			p.draw(g);
		}

		stats.draw(g);
	}

}
